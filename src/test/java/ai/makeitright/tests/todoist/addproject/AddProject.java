package ai.makeitright.tests.todoist.addproject;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

public class AddProject {
    private String EMAIL;
    private String PASSWORD;
    private String PROJECTNAME;
    private String URL;
    private WebDriver driver;

//    @BeforeClass
//    public static void beforeClass() {
//        System.setProperty("inputParameters.email","katarzyna.raczkowska@makeitright.ai");
//        System.setProperty("inputParameters.password","Automatyzacjatodo567");
//        System.setProperty("inputParameters.projectname","Kasia");
//        System.setProperty("inputParameters.url","https://todoist.com/users/showLogin");
//    }


    @Before
    public void before() {
        EMAIL = System.getProperty("inputParameters.email");
        PASSWORD = System.getProperty("inputParameters.password");
        PROJECTNAME = System.getProperty("inputParameters.projectname");
        URL = System.getProperty("inputParameters.url");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test
    public void test() throws InterruptedException {
        driver.get(URL);
        Assert.assertEquals("Current URL address '" + driver.getCurrentUrl() + "' is not like expected '" + URL + "'",URL,driver.getCurrentUrl());
        logIn();
        String projectName = addProject();
        Assert.assertTrue(isProjectVisible(projectName));
//        driver.findElement(By.xpath("//button[@data-testid='quick-add-task-action']")).click();
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String taskName = TASKNAME + " " + formatter.format(new GregorianCalendar().getTime());
//        driver.findElement(By.xpath("//input[@data-testid='add-task-content']")).sendKeys(taskName);
//
//        driver.findElement(By.xpath("//button[@data-testid='add-task']")).click();
//
//        Assert.assertTrue(isTaskAtInbox(taskName));
//
        JSONObject obj = new JSONObject();
        obj.put("projectname", projectName);
        System.setProperty("output", obj.toString());
        driver.close();

    }

    private String addProject() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String projectName = PROJECTNAME + " " + formatter.format(new GregorianCalendar().getTime());
        driver.findElement(By.xpath("//a[@class='action sel_add_project']")).click();
        driver.findElement(By.id("edit_project_modal_field_name")).sendKeys(projectName);
        driver.findElement(By.xpath("//*[@class='ist_button ist_button_red']")).click();
        return projectName;
    }

    private boolean isProjectVisible(String projectName) {
        List<WebElement> tableRows = driver.findElements(By.xpath("//ul[@id='projects_list']/li//tbody//td[2]"));
        if(tableRows.size() > 0) {
            for(WebElement row : tableRows) {
                WebElement rowTaskName = row.findElement(By.xpath("./span"));
                if (rowTaskName.getText().equals(projectName)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void logIn() throws InterruptedException {
        driver.findElement(By.id("email")).sendKeys(EMAIL);
        driver.findElement(By.id("password")).sendKeys(PASSWORD);
        driver.findElement(By.xpath("//button[@class='submit_btn ist_button ist_button_red sel_login']")).click();
        Thread.sleep(5000);
//        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='action sel_add_project']")));
    }
}
