package ai.makeitright.tests.todoist.readtitleofprevargpage;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class ReadTitleOfPage {

    private String parameterToReturn;
    private WebDriver driver;

    @Before
    public void before() {
        parameterToReturn = System.getProperty("previousResult.parameterToReturn");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test
    public void test() {
            driver.get(parameterToReturn);
            String pageTitle = driver.getTitle();
            JSONObject obj = new JSONObject();
            obj.put("titileofpage", pageTitle);
            obj.put("parameterToReturn", "task3");
            System.setProperty("output", obj.toString());

    }



}
