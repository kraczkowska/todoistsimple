package ai.makeitright.tests.todoist.quickaddtask;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuickAddTask {
    private String TASKNAME;
    private String URL;
    private WebDriver driver;

    @Before
    public void before() {
        TASKNAME = System.getProperty("inputParameters.taskname");
        URL = System.getProperty("inputParameters.url");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--window-size=1400,600");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test
    public void test() throws IOException {
        System.out.println("Start test");
        driver.get(URL);
        Assert.assertEquals("Current URL address '" + driver.getCurrentUrl() + "' is not like expected '" + URL + "'",URL,driver.getCurrentUrl());
        File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String filePath = System.getProperty("SCREENSHOTS_PATH") + System.getProperty("file.separator") + "page.png";
        System.out.println("filePath: " + filePath);
        FileUtils.copyFile(file, new File(filePath));
//        driver.findElement(By.xpath("//button[@data-testid='quick-add-task-action']")).click();
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        TASKNAME = TASKNAME + " " + formatter.format(new GregorianCalendar().getTime());
//        driver.findElement(By.xpath("//input[@data-testid='add-task-content']")).sendKeys(TASKNAME);
//
//        driver.findElement(By.xpath("//button[@data-testid='add-task']")).click();
//
//        Assert.assertTrue(isTaskAtInbox(TASKNAME));

    }

    @After
    public void prepareJson() {
        JSONObject obj = new JSONObject();
        obj.put("taskname", TASKNAME);
        obj.put("parameterToReturn", "task1");
        System.setProperty("output", obj.toString());
        driver.close();
    }

    private boolean isTaskAtInbox(String taskName) {
        List<WebElement> tableRows = driver.findElements(By.xpath("//ul[@class='tasks__list']/li"));
        if(tableRows.size() > 0) {
            for(WebElement row : tableRows) {
                WebElement rowTaskName = row.findElement(By.xpath("./span"));
                if (rowTaskName.getText().equals(taskName)) {
                    return true;
                }
            }
        }
        return false;
    }

}
