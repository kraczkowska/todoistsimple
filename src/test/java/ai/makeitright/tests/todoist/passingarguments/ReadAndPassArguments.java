package ai.makeitright.tests.todoist.passingarguments;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ReadAndPassArguments {

    private String TASKNAME;
    private String parameterToReturn;
    private WebDriver driver;

    @Before
    public void before() {
        //TASKNAME = System.getProperty("inputParameters.taskname");
        parameterToReturn = System.getProperty("previousResult.parameterToReturn");
        TASKNAME = System.getProperty("previousResult.taskname");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--window-size=1400,600");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();

    }

    @Test
    public void test() {
        System.out.println("Start test");
        driver.get("https://www.google.pl/");
        JSONObject obj = new JSONObject();
        obj.put("taskname", TASKNAME+"||task2");
        obj.put("parameterToReturn", "https://www.wp.pl/");
        System.setProperty("output", obj.toString());
        driver.close();
    }
}
