package ai.makeitright.tests.todoist.marktaskasdone;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class MarkTaskAsDone {
    private String TASKNAME;
    private String URL;
    private WebDriver driver;

    @Before
    public void before() {
        TASKNAME = System.getProperty("inputParameters.taskname");
        URL = System.getProperty("inputParameters.url");
        TASKNAME = System.getProperty("previousResult.taskname");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test
    public void test() {
        driver.get(URL);
        Assert.assertEquals("Current URL address '" + driver.getCurrentUrl() + "' is not like expected '" + URL + "'", URL, driver.getCurrentUrl());

        Assert.assertTrue(markTaskDone(TASKNAME));

        Assert.assertFalse(isTaskAtInbox(TASKNAME));

        System.setProperty("output", "{\"taskdeleted\":\"ok\"}");
        driver.close();
    }

    private boolean isTaskAtInbox(String taskName) {
        List<WebElement> tableRows = driver.findElements(By.xpath("//ul[@class='tasks__list']/li"));
        if(tableRows.size() > 0) {
            for(WebElement row : tableRows) {
                WebElement rowTaskName = row.findElement(By.xpath("./span"));
                if (rowTaskName.getText().equals(taskName)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean markTaskDone(String taskName) {
        new WebDriverWait(driver, 15).until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//ul[@class='tasks__list']/li"),0));
        List<WebElement> tableRows = driver.findElements(By.xpath("//ul[@class='tasks__list']/li"));

        for(WebElement row : tableRows) {
            WebElement rowChboxTask = row.findElement(By.xpath(".//div/span"));
            WebElement rowTaskName = row.findElement(By.xpath("./span"));
            if (rowTaskName.getText().equals(taskName)) {
                rowChboxTask.click();
                return true;
            }
        }

        return false;
    }
}
